#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

void
random_int_vector(double* v, int n)
{
  for (int i = 0; i < n; ++i) {
    v[i] = (double)rand() / RAND_MAX;
  }
}

int
main(int argc, char** argv)
{
  MPI_Init(&argc, &argv);
  int root = 0; // this processo will be the root
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  
  int n = 65536;
  double * v = NULL;
  // only the roots fills the initial array v
  if (rank == root) {
    printf("Size: %d\n", n);
    srand(time(NULL));
    // creates and fill with random values an array of int
    v = malloc(sizeof(double)*n);
    random_int_vector(v, n);
  }
  // each process prepare the buffer
  // we assume for convenvenience that size divides n
  int k = n / size;
  double * part_v = malloc(k*sizeof(double));

  // scatter portions of the array
  MPI_Scatter(v, k, MPI_DOUBLE, part_v, k, MPI_DOUBLE, root, MPI_COMM_WORLD);

  // perform summation (each process)
  double local_sum = 0;
  for (int i = 0; i < k; ++i) {
    local_sum += part_v[i];
  }

  printf("%d pSum: %f\n", rank, local_sum);

  // reduce the partial sums
  double s;
  MPI_Reduce(&local_sum, &s, 1, MPI_DOUBLE, MPI_SUM, root, MPI_COMM_WORLD);

  // each process frees its buffer
  free(part_v);
  // only the root prints the final result
  if (rank == root) {
    printf("Sum: %f\n", s);
    free(v);
  }

  MPI_Finalize();
  return 0;
}


#include <omp.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

void
random_int_vector(int* v, int n)
{
  for (int i = 0; i < n; ++i) {
    v[i] = rand();
  }
}

int
main(int argc, char** argv)
{
  
  int n = 65536;
  if (argc > 1) {
    n = atoi(argv[1]);
  }
  printf("Size: %d\n", n);
  srand(time(0));
  // creates and fill with random values an array of int
    int* v = malloc(sizeof(int)*n);
  random_int_vector(v, n);
   int s = 0;
#pragma omp parallel for reduction(+:s)
  for (int i = 0; i < n; ++i) {
    s += v[i];
  }

  printf("Sum: %d\n", s);

  free(v);
  return 0;
}

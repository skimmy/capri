#include <string>
#include <iostream>
#include <fstream>

void
load_matrix_from_file(const std::string& file_name, double* &buff, size_t &m, size_t &n) {
	std::ifstream ifs(file_name, std::ios::binary | std::ios::in);
	ifs.read((char*)&m, sizeof(size_t));
	ifs.read((char*)&n, sizeof(size_t));
	buff = new double[m*n];
	size_t i = 0;
	while(i < m*n) {
		ifs.read((char*)&buff[i], sizeof(double));
		i++;
	}
	ifs.close();
}

void
print_matrix(const double* buff, size_t m, size_t n, char sep = ',')
{
	for (size_t i = 0; i < m; ++i) {
		for (size_t j = 0; j < n; ++j) {
			std::cout << buff[i*n + j] << sep;
		}
		std::cout << "\n";
	}
}

int main(int argc, char const *argv[])
{
	double *M;
	size_t m, n;
	load_matrix_from_file(argv[1], M, m, n);
	print_matrix(M, m, n);
	return 0;
}
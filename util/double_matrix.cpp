#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <random>

int
main(int argc, char** argv)
{
  if (argc < 4) {
    std::cerr << "\n\tUsage dbmatrix <filename> <rows> <columns>\n";
    std::exit(1);
  }
  std::string filename = argv[1];
  std::size_t n = (size_t) std::atoi(argv[2]);
  std::size_t m = (size_t) std::atoi(argv[3]);
  std::uniform_real_distribution<double> unif(0, 1);
  std::default_random_engine re;

  std::ofstream ofs(filename, std::ios::out | std::ios::binary);
  ofs.write((char*)&n, sizeof(size_t));
  ofs.write((char*)&m, sizeof(size_t));

  for (int i = 0; i < n; ++i) {
    for (int j =0; j < m; ++j) {
      double d = unif(re);
      ofs.write((char*)&d, sizeof(double));
    }
  }
  
  ofs.close();
  
  return 0;
}

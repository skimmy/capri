#include <omp.h>
#include <stdio.h>
#include <fstream>
#include <limits>

int
main(int argc, char** argv)
{
  std::string filename(argv[1]);
  std::size_t n,m; // these will contain the dimension of the matrix
  double * v; // this will contain the 'linearized matrix'

  // enter the parallel region
#pragma omp parallel
  {
  int thread_num = omp_get_thread_num();
  printf("Thread %d started\n", thread_num);

#pragma omp master
  {
  int master_num = omp_get_thread_num();
  printf("Master is thread number %d\n", master_num);
  std::ifstream ifs(filename, std::ios::in | std::ios::binary);
  ifs.read((char*)&n, sizeof(std::size_t));
  ifs.read((char*)&m, sizeof(std::size_t));
  v = new double[n*m];
  for (std::size_t i = 0; i < n*m; ++i) {
    ifs.read((char*)&v[i], sizeof(double));
  ifs.close();
  } // MASTER ENDS
  
  printf("n %d\n", n);
  printf("m %d\n", m);
  

  // here all threads execute the code
  double sum = 0;
  int N = (int)(n*m);
#pragma omp parallel for reduction(+:sum)
  for (int i = 0; i < N; i++) {
    sum += v[i];
  }
  // we don't want to see as many outputs as there are threads
  // also we don't want all threads to free the resources  
#pragma omp master
  {
  printf("Average value %f\n", sum / (double)N);
  // never forget to delete / free resources...
  delete[] v;
  }
  }
  // an alternative is to put delete and output here (outside the
  // parallel region), but also corresponding declarations should
  // have been put before starting the parallel region.

  return 0;
}

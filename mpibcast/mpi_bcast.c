#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv)
{
  MPI_Init(&argc, &argv);
  double pi = 0.0;
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (rank == 0) {
    pi = 3.1415;
  }
  MPI_Bcast(&pi, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  printf("Proc. %d, Pi = %f\n", rank, pi);
  
  MPI_Finalize();
  return 0;
}

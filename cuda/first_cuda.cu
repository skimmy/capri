#include <stdio.h>
#include <iostream>

// Kernel definition
__global__ void
VecAdd(float* A, float* B, float* C)
{
    int i = threadIdx.x;
    C[i] = A[i] + B[i];
}


int
main()
{
  int N = 3;
  float* h_A = (float*)malloc(N*sizeof(float));
  float* h_B = (float*)malloc(N*sizeof(float));
  float* h_C = (float*)malloc(N*sizeof(float));
  float* d_A; 
  cudaMalloc(&d_A, N*sizeof(float));
  float* d_B; 
  cudaMalloc(&d_B, N*sizeof(float));
  float* d_C; 
  cudaMalloc(&d_C, N*sizeof(float));

  h_A[0] = 1; h_A[1] = 2; h_A[2] = 3;
  h_B[0] = 0.5; h_B[1] = -0.5; h_B[2] = 0.5;


  cudaMemcpy(d_A, h_A, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(d_B, h_B, N*sizeof(float), cudaMemcpyHostToDevice);

  VecAdd<<<1, N>>>(d_A, d_B, d_C);

  cudaMemcpy(h_C, d_C, N*sizeof(float), cudaMemcpyDeviceToHost);

  std::cout << h_C[0] << ", " << h_C[1] << ", " << h_C[2] << "\n";

  cudaFree(d_A);
  cudaFree(d_B);
  cudaFree(d_C);

  free(h_A);
  free(h_B);
  free(h_C);
}

#define N 1024

__global__ void MatAdd(float A[N][N], float B[N][N], float C[N][N])
{
    int i = threadIdx.x;
    int j = threadIdx.y;
    C[i][j] = A[i][j] + B[i][j];
}

__global__ void MatAddBlocks(float A[N][N], float B[N][N], float C[N][N])
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    if (i < N && j < N)
        C[i][j] = A[i][j] + B[i][j];
}



int main()
{
	// WARNING: this is made easy to let nvcc compile,
	// but it does not allocate on the GPU for which
	// one should use cudaMalloc!!
	// --> You'll get SEGFAULT ...
	float d_A[N][N]; 
  	float d_B[N][N]; 
  	float d_C[N][N]; 

	// 1 block of NxN threads
	dim3 threadsPerBlock(N, N);
  	MatAdd<<<1, threadsPerBlock>>>(d_A, d_B, d_C);

  	// A grid of NxN sized blocks such that NxN matrix is
  	// all mapped to threads
  	dim3 threadsPerBlock2(16, 16);
    dim3 numBlocks(N / threadsPerBlock2.x, N / threadsPerBlock2.y);
    MatAdd<<<numBlocks, threadsPerBlock2>>>(d_A, d_B, d_C);

  	
  	return 0;
}

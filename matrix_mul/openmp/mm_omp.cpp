#include <string>
#include <iostream>
#include <fstream>
#include <random>

#include <omp.h>

using namespace std;


void
make_random_matrix(double* &buff, size_t m, size_t n) 
{
	buff = new double[m*n];
	std::uniform_real_distribution<double> unif(0, 1);
	std::default_random_engine re;
	for (int i = 0; i < n; ++i) {
    	for (int j =0; j < m; ++j) {
      		buff[i*n + j] = unif(re);
    	}
  	}
}


/* reads */
void
load_matrix_from_file(const string& file_name, double* &buff, size_t &m, size_t &n) {
	ifstream ifs(file_name, ios::binary | ios::in);
	ifs.read((char*)&m, sizeof(size_t));
	ifs.read((char*)&n, sizeof(size_t));
	buff = new double[m*n];
	size_t i = 0;
	while(i < m*n) {
		ifs.read((char*)&buff[i], sizeof(double));
		i++;
	}
	ifs.close();
}

void 
save_matrix_to_file(const string& file_name, double* buff, size_t m, size_t n) {
	ofstream ofs(file_name, ios::binary | ios::out);
	ofs.write((char*)&m, sizeof(size_t));
	ofs.write((char*)&n, sizeof(size_t));
	size_t i = 0;
	while(i < m*n) {
		ofs.write((char*)&buff[i], sizeof(double));
		++i;
	}
	ofs.close();
}

int
main(int argc, char const *argv[])
{
	if (argc < 3) {
		cerr << "\n USAGE:\n\t " << argv[0] << "m n l [file_out]\n";
		exit(1);
	}
	// We use matrix 'flattened' into one dimensional arrays
	// and stored in 'row major'. For example
	// | 1  2 |
	// | 3  4 |
	// becomes
	// | 1 2 3 4 |
	// If the matrix is m x n, the elements (i,j) is in position
	//   i*n + j
	// in the flattened vector
	double *A, *B, *C;
	size_t mA = atoi(argv[1]); 
	size_t nA = atoi(argv[2]);
	size_t mB = nA;
	size_t nB = atoi(argv[3]);
	size_t mC = mA;
	size_t nC = nB;

	make_random_matrix(A, mA, nA);
	make_random_matrix(B, mB, nB);
	

	C = new double[mA*nB];

#pragma omp parallel
{
	// we leave OpenMP to decide how to split the lines
	// of C and assign to different threads
    #pragma omp for
    for (int i = 0; i < mA; ++i) // parallel: scan rows
    {
    	for (int j = 0; j < nB; ++j) // serial: scan cols
    	{
    		C[i*nB + j] = 0;
    		for (int k = 0; k < nA; ++k) // serial: dot product
    		{
    			C[i*nC + j] += A[i*nA + k] * B[k*nB + j];
			}
    	}
    }
}

	if (argc > 4) {
		save_matrix_to_file(argv[4], C, mA, nB);
	}

	delete[] A;
	delete[] B;
	delete[] C;
	return 0;
}
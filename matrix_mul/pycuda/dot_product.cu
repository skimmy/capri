#include <string>
#include <iostream>
#include <fstream>
#include <random>

using namespace std;


void
make_random_matrix(float* &buff, size_t m, size_t n) 
{
	buff = new float[m*n];
	std::uniform_real_distribution<float> unif(0, 1);
	std::default_random_engine re;
	for (int i = 0; i < n; ++i) {
    	for (int j =0; j < m; ++j) {
      		buff[i*n + j] = unif(re);
    	}
  	}
}

__global__ void DotProduct(float * A, float * B, float * C, int m, int n, int l)
{
    int i = threadIdx.x + blockDim.x * blockIdx.x;
    int j = threadIdx.y + blockDim.x * blockIdx.x;
    C[i*l + j] = 0;
    for (int k = 0; k < n; ++k) {
    	C[i*l + j] += A[i*n + k]*B[k + j*l];
    }
    // Notice that 'm' is never used
    // it is implied by the blocks
}

int main(int argc, char** argv)
{
	// Use float which is more performant on GPUs
	float *A, *B, *C;
	size_t mA = atoi(argv[1]); 
	size_t nA = atoi(argv[2]);
	size_t mB = nA;
	size_t nB = atoi(argv[3]);
	size_t mC = mA;
	size_t nC = nB;

	make_random_matrix(A, mA, nA);
	make_random_matrix(B, mB, nB);
	

	C = new float[mA*nB];

	DotProduct<<<mC, nC>>>(A, B, C, mA, nA, nB);

	return 0;

}

import sys
import numpy as np

import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule

mod = SourceModule("""
__global__ void DotProduct(float * A, float * B, float * C, int * dims)
{
    int m = dims[0];
    int n = dims[1];
    int l = dims[2];
    int i = threadIdx.x + blockDim.x * blockIdx.x;
    int j = threadIdx.y;
    C[i*l + j] = 0;
    for (int k = 0; k < n; ++k) {
    	C[i*l + j] += A[i*n + k]*B[k + j*l];
    }
}
""")

m = int(sys.argv[1])
n = int(sys.argv[2])
l = int(sys.argv[3])

if (m*l % 1024) != 0:
    print("m * l must be a multiple of 1024")
    sys.exit(2)

blocks = int(m*l/1024)

dims = np.asarray([m, n, l], dtype=np.int32)
A = np.random.randn(m*n).astype(np.float32)
B = np.random.randn(n*l).astype(np.float32)
C = np.empty(m*l).astype(np.float32)


if len(sys.argv) > 4:
    print("m   ", m)
    print("n   ", n)
    print("l   ", l)
    print("b   ", blocks)
    print("|A| ", len(A))
    print("|B| ", len(B))
    print("|C| ", len(C))

# A_d = cuda.mem_alloc(A.nbytes)
# B_d = cuda.mem_alloc(B.nbytes)
# C_d = cuda.mem_alloc(C.nbytes)

# cuda.memcpy_htod(A_d, A)
# cuda.memcpy_htod(B_d, B)

func = mod.get_function("DotProduct")
func(cuda.In(A), cuda.In(B), cuda.Out(C),
     cuda.In(dims), block=(m, n, 1), grid=(blocks, 1))
#func(A_d, B_d, C_d, m, n, l, block=(m, n, 1))

#cuda.memcpy_dtoh(C, C_d)

if len(sys.argv) > 4:
    print(A)
    print(B)
    print(C)



import sys
import numpy as np
from mpi4py import MPI


def random_matrix(m, n, seed=None):
	if seed:
		np.random_state = seed
	return np.random.randn(m, n)


def main():
	if len(sys.argv) < 4:
		print("Usage {0} m n l".format(sys.argv[0]))
		sys.exit(1)
	DEBUG=None
	TEST=None
	if len(sys.argv) > 4:
		DEBUG=True
		if int(sys.argv[4]) > 1:
			TEST=True
	# Always good idea to name constants
	ROOT_PROCESS=0
	comm = MPI.COMM_WORLD
	P = comm.Get_size()
	rank = comm.Get_rank()

	# generats random matrices from sizes
	# given as input 
	#   A -> m x n
	#   B -> n x l
	m = int(sys.argv[1])
	n = int(sys.argv[2])
	l = int(sys.argv[3])

	# Some parameters check
	# For less messy code
	if m % P != 0:
		print("m must be a multiple of the number of processes")
		sys.exit(2)

	# convenience variables
	rows_per_process = int(m / P)
	data_per_process = rows_per_process * n

	# These need to be defined in *all* processes
	B = None 
	A = None
	C = None


	# It is more convenient to work with "flatten"
	# matrixes: matrixes stored in linear array
	if rank == ROOT_PROCESS: # only the root "loads" file
		A = random_matrix(m, n).flatten()
		B = random_matrix(n, l).flatten()
		if TEST:
			A = np.zeros([m, n])
			np.fill_diagonal(A, 2)
			A = A.flatten()
			B = np.zeros([n, l])
			np.fill_diagonal(B, -1)
			B = B.flatten()
		C = np.empty((m,l))

	# computes the counts and displacements for MPI_Scatterv
	count = np.full(P, data_per_process).astype(np.int32)
	displ = np.arange(0, m*n, step=data_per_process).astype(np.int32)
	
	# broadcast and re-shape matrix B
	B = comm.bcast(B, root=ROOT_PROCESS)
	B = np.asarray(B).reshape(n,l)
	

	Arecv = np.empty(data_per_process, dtype=np.float64)
	#Arecv = comm.scatter(A, root=ROOT_PROCESS)
	comm.Scatterv([A, count, displ, MPI.DOUBLE], Arecv, root=ROOT_PROCESS)
	# reshape received objects into a matrix
	Arecv = Arecv.reshape(rows_per_process, n)

	if DEBUG:
		print("\nProcess {0}\n  Shape A -> {1}\n  Shape B -> {2}".format(rank, Arecv.shape, B.shape))
		if TEST:
			print(Arecv)
		print()

	# Make it simple and use numpy algorithms
	# Notice: each process does this with different Arecv
	Cpart = np.matmul(Arecv, B)
	if TEST:
		print(Cpart)


	# Need to re-conpute count and displacements
	count = np.full(P, rows_per_process*l)
	displ = np.arange(0, m*l, step=rows_per_process*l).astype(np.int32)
	comm.Gatherv(Cpart.flatten(), [C, count, displ, MPI.DOUBLE], root=ROOT_PROCESS)
	

	if rank == ROOT_PROCESS and DEBUG:
		C = C.reshape(m, l)
		test_matrix = np.matmul(A.reshape(m, n),B.reshape(n, l))
		print(np.allclose(test_matrix, C))



if __name__ == "__main__":
	main()
